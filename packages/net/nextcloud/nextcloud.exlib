# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'nextcloud-12.0.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

export_exlib_phases src_install

SUMMARY="Personal cloud that runs on your own server"
HOMEPAGE="https://nextcloud.com"

if [[ ${PV} == *beta* ]] || [[ ${PV} == *RC* ]] ; then
    DOWNLOADS="https://download.nextcloud.com/server/prereleases/${PNV}.tar.bz2"
else
    DOWNLOADS="https://download.nextcloud.com/server/releases/${PNV}.tar.bz2"
fi

LICENCES="AGPL-3"
MYOPTIONS="
    ( mysql [[ description = [ Use MySQL/MariaDB with Nextcloud ] ]]
    postgresql [[ description = [ Use PostgreSQL with Nextcloud ] ]]
    sqlite [[ description = [ Use SQLite with Nextcloud ] ]]
    ) [[ number-selected = at-least-one ]]
"

WORK="${WORKBASE}/${PN}"

# Nextcloud <13.0.0beta1 doesn't work with PHP 7.2
if ! ever at_least 13.0.0beta1 ; then
    DEPENDENCIES="
        run:
            dev-lang/php:*[<7.2][php_extensions:curl][php_extensions:filter][php_extensions:gd]
            dev-lang/php:*[<7.2][php_extensions:xml][php_extensions:ssl][php_extensions:fileinfo]
            mysql? (
                dev-lang/php:*[<7.2][php_extensions:pdo-mysql]
                virtual/mysql
            )
            postgresql? (
                dev-db/postgresql
                dev-lang/php:*[<7.2][php_extensions:pdo-pgsql]
            )
            sqlite? (
                dev-db/sqlite
                dev-lang/php:*[<7.2][php_extensions:pdo-sqlite]
            )
    "
else
    DEPENDENCIES="
        run:
            dev-lang/php:*[php_extensions:curl][php_extensions:filter][php_extensions:gd]
            dev-lang/php:*[php_extensions:xml][php_extensions:ssl][php_extensions:fileinfo]
            mysql? (
                dev-lang/php:*[php_extensions:pdo-mysql]
                virtual/mysql
            )
            postgresql? (
                dev-db/postgresql
                dev-lang/php:*[php_extensions:pdo-pgsql]
            )
            sqlite? (
                dev-db/sqlite
                dev-lang/php:*[php_extensions:pdo-sqlite]
            )
    "
fi

nextcloud_src_install() {
    if [[ ${PNV} < 13 ]]; then
        insinto /usr/share/webapps/${PNVR}
    else
        insinto /usr/share/webapps/${PN}
    fi

    doins -r .
}

