# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ tag=v${PV} ] openrc-service s6-rc-service
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require python [ blacklist=none multibuild=false ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Apparmor security system"
DESCRIPTION="
AppArmor is a Mandatory Access Control (MAC) system which is a kernel (LSM)
enhancement to confine programs to a limited set of resources. This package
includes all of its user-space utilities and profiles.
"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS="
    extra-profiles [[
        description = [ Add extra downstream profiles (e.g. for pulseaudio) ]
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config [[ note = [ for libapparmor ] ]]
    build+run:
        dev-lang/perl:=
        dev-lang/swig [[ note = [ for libapparmor ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/use-prefixed-tools.patch
    "${FILES}"/use-correct-sbin.patch
)

apparmor_src_prepare() {
    default

    edo pushd libraries/libapparmor
    eautoreconf
    edo popd

    # Apparmor makes heavy use of cpp to detect what's defined in
    # some headers like capabilities.h. It _doesn't_ use it for
    # compilation
    mkdir "${WORKBASE}"/bin
    edo ln -s /usr/$(exhost --target)/bin/cpp "${WORKBASE}"/bin
    export PATH="${WORKBASE}/bin:$PATH"

    if option extra-profiles; then
        edo cp -r "${FILES}"/extra-profile/* profiles/apparmor.d
    fi
}

apparmor_src_configure() {
    edo pushd libraries/libapparmor
    econf --with-perl --with-python
    edo popd
}

apparmor_src_compile() {
    emake -C libraries/libapparmor
    emake -C parser
    emake -C utils
    emake -C binutils
    emake -C profiles
}

apparmor_src_install() {
    # FIX ME: Buggy makefiles don't create the dir if we
    # install systemd files and instead copy the systemd
    # function file to this location
    dodir /usr/$(exhost --target)/lib/apparmor

    emake -C libraries/libapparmor DESTDIR="${IMAGE}" install
    emake -C parser DISTRO="unknown" SBINDIR="${IMAGE}/usr/$(exhost --target)/bin" APPARMOR_BIN_PREFIX="${IMAGE}/usr/$(exhost --target)/lib/apparmor" DESTDIR="${IMAGE}" SYSTEMD_UNIT_DIR="${IMAGE}/usr/$(exhost --target)/lib/systemd/system" install install-systemd
    emake -C utils BINDIR="${IMAGE}/usr/$(exhost --target)/bin" DESTDIR="${IMAGE}" install
    emake -C binutils BINDIR="${IMAGE}/usr/$(exhost --target)/bin" DESTDIR="${IMAGE}" install
    emake -C profiles DESTDIR="${IMAGE}" install

    dodir /etc/apparmor.d/disable
    keepdir /etc/apparmor.d/disable

    keepdir /var/lib/apparmor

    edo chmod 755 "${IMAGE}"/usr/$(exhost --target)/lib/apparmor/apparmor.systemd

    # TODO: Their custom python script thingie installs into these paths :(
    edo mv "${IMAGE}/usr/bin/aa-easyprof" "${IMAGE}/usr/$(exhost --target)/bin"
    edo rmdir "${IMAGE}/usr/bin"
    edo cp -r "${IMAGE}/usr/lib/python$(python_get_abi)/" "${IMAGE}/usr/$(exhost --target)/lib/python$(python_get_abi)"
    edo rm -r "${IMAGE}/usr/lib"

    install_openrc_files
    install_s6-rc_files
}

